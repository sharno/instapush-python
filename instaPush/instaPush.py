import json
import requests

def instaPush(appID, secret, activity, trackers):
	url = "http://api.instapush.im/post"
	headers = {"X-INSTAPUSH-APPID": appID, 'X-INSTAPUSH-APPSECRET': secret, 'Content-Type': 'application/json'}
	data = {"event": activity, "trackers": trackers}

	response = requests.post(url, headers=headers, data=json.dumps(data))
	return response