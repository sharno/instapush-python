"""
InstaPush library
http://instapush.im
"""

__title__ = 'instaPush'
__version__ = '0.1'
__author__ = 'sharno'
__license__ = 'MIT License'
__copyright__ = 'Copyright 2013 sharno'

from instaPush import *